## Main config

# Provider
provider "aws" {
    region = "${var.aws_region}" 
}

# Create vpc
resource "aws_vpc" "gitlab-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = { 
      Name = "Gitlab Demo"
  }
}

# Create Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.gitlab-vpc.id
}

# Create Route table
resource "aws_route_table" "prod-route-table" {
  vpc_id = aws_vpc.gitlab-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "Gitlab-demo"
  }
}

# Create a Subnet
resource "aws_subnet" "subnet-1" {
    vpc_id = aws_vpc.gitlab-vpc.id
    cidr_block = "10.0.1.0/24"
    availability_zone = "${var.aws_availability_zone}"

    tags = {
        Name = "gitlab-subnet"
    }
}

# Associate Subnet with Routed Table
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.prod-route-table.id
}

# Create a Security Group to allow port 22,80,443
resource "aws_security_group" "allow_web" {
  name        = "allow_web_traffic"
  description = "Allow Web inbound traffic"
  vpc_id      = aws_vpc.gitlab-vpc.id

  ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_web"
  }
}

# Create a network interface with an ip in the subnet
resource "aws_network_interface" "gitlab-server-nic" {
  subnet_id       = aws_subnet.subnet-1.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_web.id]
}

# Assign an elastic IP to the network interface
resource "aws_eip" "one" {
  domain                    = "vpc"
  network_interface         = aws_network_interface.gitlab-server-nic.id
  associate_with_private_ip = "10.0.1.50"
}

resource "aws_route53_record" "www" {
  zone_id = "Z089511416J7BA28AN84S"
  name    = "${var.route53_record}"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.one.public_ip]
}

