# Gitlab Demo

## What this project does

The project builds a Gitlab environment on AWS using terraform and Puppet.

## Quick start

Trigger the pipeline on the [pipelines](https://gitlab.com/pic16c7x/gitlab_demo/-/pipelines) page;

1. Validate - This validates the Terraform code.
2. Plan - This builds the plan to deploy.
3. Apply - Deploys the environment - **manual step** 
4. Destroy - Destroys the environment and cleans everything up - **manual step**

The environment takes approximately 15 minutes to build, once completed it will be available here [git.pic16c7x.com](https://git.16c7x.com/users/sign_in).

## More detailed explanation

This project contains the Terraform and Gitlab CI code used to build the environment.

### Terraform code

* inputs.tf - Specifies user configurable parameters that may be modified, they are grouped into one file to make finding them easier.
* instance.tf - Describes the EC2 instance that will be created and the configuration script which uses [user-data](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html). The script does the following;
  * Downloads the Puppet agent and installs it.
  * Installs git as the AMI doesn't have it. 
  * Installs the required Puppet modules.
  * Clones and applies the [gitlab_profile](https://gitlab.com/pic16c7x/gitlab_profile).
* main.tf - This file describes the VPC and network configuration.
* outputs.tf - This specifies the following parameters at the end of the application of the Terraform code;
  * Elastic IP - Use this IP to ssh directly into the EC2 instance.

### Gitlab CI code

* .gilab-ci.yml - This contains the following sections;
  * variables - Artifact names in which to save the plan and the state.
  * image - This specifies the Docker image used to run the commands in the build stages.
  * before script - This contains the AWS keys which are stored as secrets in [settings -> CI/CD -> variables](https://gitlab.com/pic16c7x/gitlab_demo/-/settings/ci_cd).
  * validate - This runs the Terraform Docker container, the project is downloaded and the command ```terraform validate``` to verify the code is valid.
  * plan - Creates an execution plan to review before the the apply stage, it create a STATE and PLAN artifact.
  * apply - This runs the Terraform Docker container to deploy the PLAN and update the STATE.
  * Destroy - This runs the Terraform Docker and passes in the STATE artifact to destroy the environment. 


## Debugging

If Gitlab fails to come up, try giving it another 5 minutes, it takes time to download the installation file and apply the Puppet code. If this does not work then logging into the EC2 instance will be required, this does require an ssh key pair to check the /var/build.log file, all the output from the user-data scrip is directed here.

## Improvements

Things that would make this project better.

1. A CI stage that would connect to the the URL and verify the Gitlab server was up within a specified time period.
2. Configure backups and save them into an S3 bucket.
3. Integrate with an SSO