## Instance

resource "aws_instance" "web-server-instance" {
  ami           = "${var.aws_ami}"
  instance_type = "t2.large"
  availability_zone = "${var.aws_availability_zone}"
  key_name = "andrew"

  network_interface {
    device_index = 0
    network_interface_id = aws_network_interface.gitlab-server-nic.id
  }

  user_data = <<-EOF
              #!/bin/bash
              sudo echo "########################################" >> /var/build.log
              sudo echo "# Downloading Puppet agent" >> /var/build.log
              sudo echo "########################################" >> /var/build.log
              sudo curl -JLO 'https://pm.puppetlabs.com/puppet-agent/2021.6.0/7.16.0/repos/el/8/puppet7/x86_64/puppet-agent-7.16.0-1.el8.x86_64.rpm' >> /var/build.log
              sudo echo "########################################" >> /var/build.log
              sudo echo "# Installing Puppet agent" >> /var/build.log
              sudo echo "########################################" >> /var/build.log
              sudo yum install -y puppet-agent-7.16.0-1.el8.x86_64.rpm >> /var/build.log
              sudo echo "########################################" >> /var/build.log
              sudo echo "# Installing Git" >> /var/build.log
              sudo echo "########################################" >> /var/build.log
              sudo yum install -y git >> /var/build.log
              sudo echo "########################################" >> /var/build.log
              sudo echo "# Installing Puppet modules" >> /var/build.log
              sudo echo "########################################" >> /var/build.log
              sudo /opt/puppetlabs/bin/puppet module install puppet-gitlab --version 8.0.0 >> /var/build.log
              sudo echo "########################################" >> /var/build.log
              sudo echo "# Cloning gitlab_profile" >> /var/build.log
              sudo echo "########################################" >> /var/build.log
              sudo git clone https://gitlab.com/pic16c7x/gitlab_profile.git /etc/puppetlabs/code/environments/production/modules/gitlab_profile >> /var/build.log
              sudo echo "########################################" >> /var/build.log
              sudo echo "# Applying Gitlab profile" >> /var/build.log
              sudo echo "########################################" >> /var/build.log              
              sudo /opt/puppetlabs/bin/puppet apply /etc/puppetlabs/code/environments/production/modules/gitlab_profile/examples/init.pp  >> /var/build.log
              EOF

  tags = {
    Name = "Gitlab-demo"
    lifetime = "1d"
  }
}